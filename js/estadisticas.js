var NUM_PEDIDO;
var ARRAY_COTIZACIONES = [];
var ARRAY_NOTA_PEDIDO = [];
var functionCot = false;
var functionNot = false;
var PRODUCTOS;
var CANTIDADPRODUCTOS;
var MES;
var YEARS;
var ARRAYMESES = [];
var NOTACLIENTES;
var NOMMAYORRENT = [];
var PORCENTAJE_NOTA_COTIZACION;
var PORCENTAJE_NOTA_PEDIDO;
var DATO_GRAFICO_VENDEDOR1_COTI;
var DATO_GRAFICO_VENDEDOR2_COTI;
var DATO_GRAFICO_VENDEDOR1_NOTA;
var DATO_GRAFICO_VENDEDOR2_NOTA;
var FECHA_1;
var FECHA_2;
var ARRAYGANANCIA;
var ARRAYUTILIDAD;
var ARRAYDIAMES;
var PROMEDIOVENTA;
var PROMEDIOGANANCIA;
var NIVEL;
var ARRAY_COTIZACIONES_PHP;
var ARRAY_NOTA_PHP;
var NOMBREPRODUCTO;

function cargar(fecha_1, fecha_2, estado, pie_chart,nivel, idVendedorLogueado) {
	var sql;
	var filtroVendedor;
	NIVEL=nivel;
	//console.error(NIVEL);
	//*-Obtener el numero del mes del calendario
	var fecha_1 = formatear_fecha(String(fecha_1));
	var fecha_2 = formatear_fecha(String(fecha_2));
	FECHA_1 = fecha_1;
	FECHA_2 = fecha_2;
	var estado;
	if (nivel > 0) {
		filtroVendedor = 'AND id_vendedor=' + idVendedorLogueado;

	} else {
		filtroVendedor = "";
	}
	if (estado == 1) {
		sql = 'SELECT COUNT(*) as venta from ventas where fechaCotizacion between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"  ' + filtroVendedor + ' ';

	}

	if (estado == 2) {
		sql = 'SELECT COUNT(*) as venta from ventas where fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" ' + filtroVendedor + ' ';

	}
	if (estado == 3) {
		sql = 'SELECT COUNT(*) as venta,sum(total) as total from ventas where fechaCompletado between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" ' + filtroVendedor + ' ';

	}

	//-*AJAX
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var arreglo = JSON.parse(data);
			var numero_cotizaciones = arreglo[0][0];
			var total_utilidad = arreglo[0]["total"];

			var utilidad_mostrar = new Intl.NumberFormat('es-MX').format(Math.round(total_utilidad));
			if (estado == 1) {
				$("#cotizaciones > p > b").html("");
				$("#cotizaciones > p > b").append(numero_cotizaciones);
				functionCot = true;
				cargar(fecha_1, fecha_2, 3, pie_chart, nivel, idVendedorLogueado);
			}
			if (estado == 2) {
				$("#nota_pedido > p > b").html("");
				$("#nota_pedido > p > b").append(+numero_cotizaciones);
				functionNot = true;
				cargar(fecha_1, fecha_2, 3, pie_chart, nivel, idVendedorLogueado);
			}
			if (estado == 3) {
				$("#completado > p > b").html("");
				$("#completado > p > b").append(numero_cotizaciones);
				if (total_utilidad == 0) {
					$("#completado > p > b").html("");
					$("#completado > p > b").append(0);
				} else {
					$("#utilidad > p > b").html("");
					$("#utilidad > p > b").append(utilidad_mostrar);

				}
				var cotizaciones = $("#cotizaciones > p > b").text();
				var nota_pedido = $("#nota_pedido > p > b").text();

				var porcentaje_cotizacion = redondeo(cotizaciones / (parseInt(cotizaciones) + parseInt(nota_pedido)), 2) * 100;

				var porcentaje_nota_pedido = redondeo(nota_pedido / (parseInt(cotizaciones) + parseInt(nota_pedido)), 2) * 100;
				PORCENTAJE_NOTA_COTIZACION = redondeo(porcentaje_cotizacion, 0);
				PORCENTAJE_NOTA_PEDIDO = redondeo(porcentaje_nota_pedido, 0);

				var totalCaptura = redondeo(parseInt(nota_pedido*100)/parseInt(cotizaciones),0);
				console.error(totalCaptura);
				var diferenciaCaptura=100-totalCaptura;
				console.info(diferenciaCaptura);

				pie_chart(redondeo(totalCaptura, 0), diferenciaCaptura);
      /*probando editor online.....
      ahora si me sale el cambio*/
			}

		},
		error: function (request, status, error) {
			console.error("Error: Could not UPDATE");
		}
	});
}
//*-cargar estadisticas
function cargar_estadisticas(fecha_1, fecha_2, nivel, idVendedorLogueado) {
	//*-obtener mes a partir de la fecha del calendario
	var fecha = new Date(fecha_2);
	var dia = 1;
	fecha.setDate(fecha.getDate() + dia);
	var numeroMes = fecha.getMonth()//*-obtener el mes
	var numeroYears = fecha.getFullYear();//*-obtener el año
	MES = numeroMes;
	YEARS = numeroYears;

	//*-limpiar array----------------
	ARRAY_COTIZACIONES = [];
	ARRAY_NOTA_PEDIDO = [];
	ARRAYMESES = [];
	//*--------------------------------------------------------------
	cargar(fecha_1, fecha_2, 1, pie_chart, nivel, idVendedorLogueado);
	cargar(fecha_1, fecha_2, 2, pie_chart, nivel, idVendedorLogueado);
	//cargams la funciones 3 cuando cargen las otras 2
	//obtener_fecha(fecha_1);
	cargarVendedor(fecha_1, fecha_2, numeroCotizacionVendedores, nivel, idVendedorLogueado);
	cargarClientes(fecha_1, fecha_2, nivel, idVendedorLogueado);

}
//*-cambiar fecha calendario
function cargar_estadistica_onchange(nivel, idVendedorLogueado) {
	document.getElementById('crear_pdf').disabled = true;
	ARRAY_COTIZACIONES = [];
	ARRAY_NOTA_PEDIDO = [];
	ARRAYMESES = [];
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_termino = $("#fecha_termino").val();
	cargar_estadisticas(fecha_inicio, fecha_termino, nivel, idVendedorLogueado);
}



//*-llamar a vendedor-----------------------------------------------------------------------
function cargarVendedor(fecha_1, fecha_2, numeroCotizacionVendedores, nivel, idVendedorLogueado) {
	var nombresVendedores = [];
	var idVendedor = [];
	var contadorCotizacion=0;
	if (nivel > 0) {
		filtroVendedor = 'WHERE id_vendedor=' + idVendedorLogueado;
		var sql = 'SELECT id_vendedor,nombreVendedor FROM vendedores ' + filtroVendedor + '  order by nombreVendedor ASC';
	} else {
		var sql = 'SELECT id_vendedor,nombreVendedor FROM vendedores  order by nombreVendedor DESC';
	}

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var arreglo = JSON.parse(data);
			for (var i = 0; i < arreglo.length; i++) {
				nombresVendedores.push(arreglo[i]['nombreVendedor']);
				idVendedor.push(arreglo[i]['id_vendedor']);
			}

			numeroCotizacionVendedores(fecha_1, fecha_2, nombresVendedores, idVendedor, numeroNotaPedido, nivel,contadorCotizacion);

		},
		error: function (request, status, error) {
			console.error('Error: Could not cargarVendedor');
		}
	});

}

/*Contar cotizaciones y notas de pedido--------------------------------------------------------*/
//*-numero de cotizaciones vendedor 1
function numeroCotizacionVendedores(fecha1, fecha2, nombresVendedores, idVendedor, numeroNotaPedido, nivel,contadorCotizacion) {
		var sql = 'SELECT COUNT(*) as venta from ventas where fechaCotizacion between "' + fecha1 + ' 00:00:00" and "' + fecha2 + ' 23:59:59" and id_vendedor=' + idVendedor[contadorCotizacion];

		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },

			success: function (data) {
				contadorCotizacion++;
				var arreglo = JSON.parse(data);
				ARRAY_COTIZACIONES.push(arreglo[0]['venta']);

				if (contadorCotizacion == idVendedor.length) {
					var contadorNotaPedido=0;
					numeroNotaPedido(fecha1, fecha2, nombresVendedores, idVendedor, nivel,contadorNotaPedido)
				}else{

					numeroCotizacionVendedores(fecha1, fecha2, nombresVendedores, idVendedor, numeroNotaPedido, nivel,contadorCotizacion);

				}
			},
			error: function (request, status, error) {
				console.error('Error: Could not documento');
			}
		});

}

//*-contar numero pedido vendedor 1
function numeroNotaPedido(fecha1, fecha2, nombresVendedores, idVendedor, nivel,contadorNotaPedido) {

		var sql = 'SELECT COUNT(*) as venta from ventas where fechaNotaPedido between "' + fecha1 + ' 00:00:00" and "' + fecha2 + ' 23:59:59" and id_vendedor=' + idVendedor[contadorNotaPedido];

		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },
			beforeSend: function (objeto) {
				$("#salidaTabla").html("<img src='imagenes/ajax-loader.gif'>");
			},
			success: function (data) {
				contadorNotaPedido++;
				var arreglo = JSON.parse(data);
				ARRAY_NOTA_PEDIDO.push(arreglo[0]['venta']);

				if (contadorNotaPedido == idVendedor.length) {
					graficoVendedores(nombresVendedores, ARRAY_COTIZACIONES, ARRAY_NOTA_PEDIDO);
					if (nivel < 1) {
						DATO_GRAFICO_VENDEDOR1_COTI = ARRAY_COTIZACIONES[0][0];
						DATO_GRAFICO_VENDEDOR2_COTI = ARRAY_COTIZACIONES[1][0];
						DATO_GRAFICO_VENDEDOR1_NOTA = ARRAY_NOTA_PEDIDO[0][0];
						DATO_GRAFICO_VENDEDOR2_NOTA = ARRAY_NOTA_PEDIDO[1][0];
						ARRAY_COTIZACIONES_PHP=ARRAY_COTIZACIONES.toString();
						ARRAY_NOTA_PHP=ARRAY_NOTA_PEDIDO.toString();

					}


				}else{
					numeroNotaPedido(fecha1, fecha2, nombresVendedores, idVendedor, nivel,contadorNotaPedido);
				}
			},
			error: function (request, status, error) {
				console.error('Error: Could not documento');
			}
		});

}
//*----------------------------------------------------------------------------------
//*Cotizaciones por clientes----------------------------------------------------------------------
//*-llamar a cliente
function cargarClientes(fecha_1, fecha_2, nivel, idVendedorLogueado) {

	var nombreCliente = [];
	var nPedidosClientes = [];
	var idCliente = [];
	if (nivel > 0) {
		var filtroVendedor = 'and v.id_vendedor=' + idVendedorLogueado;
		var sql = 'select DISTINCT v.id_cliente from ' +
			' ventas v inner join clientes c on c.id_cliente=v.id_cliente where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" ' + filtroVendedor + '';
	} else {
		var sql = 'select DISTINCT v.id_cliente from ' +
			' ventas v inner join clientes c on c.id_cliente=v.id_cliente where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" ';
	}

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var arreglo = JSON.parse(data);

			for (var i = 0; i < arreglo.length; i++) {

				idCliente.push(arreglo[i]['id_cliente']);
			}

			var contador = 0;
			cargarProductos(fecha_1, fecha_2, nivel, idVendedorLogueado)

			if (idCliente.length > 0) {
				contarNotaPedidoClientesRecursivo(fecha_1, fecha_2, nivel, idVendedorLogueado, idCliente, contador, nombreCliente, nPedidosClientes);
			} else {
				//*-si no hay datos muestro el grafico igual
				bar_chartVendedores();
					document.getElementById('crear_pdf').disabled = true;
					PROMEDIOGANANCIA=0;
					PROMEDIOVENTA=0;
					guardar_grafico1();


			}
		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});
}

//-----------------------------------------------------------------------------------------------------------
//*-contar numero de cotizaciones por clientes
function contarNotaPedidoClientesRecursivo(fecha1, fecha2, nivel, idVendedorLogueado, idCliente, contador, nombresCliente, nPedidosClientes) {

	if (nivel > 0) {

		filtroVendedor = 'and id_vendedor=' + idVendedorLogueado;
		var sql = 'SELECT COUNT(*) as venta , c.nombre_cliente  from ' +
			' ventas v inner join clientes c on c.id_cliente=v.id_cliente where fechaNotaPedido between "' + fecha1 + ' 00:00:00" and "' + fecha2 + ' 23:59:59" and c.id_cliente= ' + idCliente[contador] + ' ' + filtroVendedor;
	} else {
		var sql = 'SELECT COUNT(*) as venta , c.nombre_cliente  from ' +
			' ventas v inner join clientes c on c.id_cliente=v.id_cliente where fechaNotaPedido between "' + fecha1 + ' 00:00:00" and "' + fecha2 + ' 23:59:59" and c.id_cliente= ' + idCliente[contador];
	}

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {

			var arreglo = JSON.parse(data);

			contador++;
			for (var i = 0; i < arreglo.length; i++) {
				nPedidosClientes.push(arreglo[i]["venta"]);
				nombresCliente.push(arreglo[i]["nombre_cliente"]);
			}
			if (contador == idCliente.length) {

				bar_chartVendedores(nombresCliente, nPedidosClientes);

			} else {
				//##Recursividad
				contarNotaPedidoClientesRecursivo(fecha1, fecha2, nivel, idVendedorLogueado, idCliente, contador, nombresCliente, nPedidosClientes);
			}


		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});

}
//------------------------------------------------------------------------------------------


//*-cargar Productos -----------------------------------------------------
function cargarProductos(fecha_1, fecha_2, nivel, idVendedorLogueado) {
	codigoProducto = [];
	nombreProducto = [];
	productosVendidos = [];
	var sql = 'SELECT DISTINCT vr.codigoProducto,vr.nombreProducto FROM ' +
		' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta ' +
		' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" group by vr.codigoProducto';

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {

			var arreglo = JSON.parse(data);
			for (var i = 0; i < arreglo.length; i++) {
				codigoProducto.push(arreglo[i]['codigoProducto']);
			}
			var contador = 0;
			if (codigoProducto.length > 0) {
				ProductosMasVendidos(fecha_1, fecha_2, nivel, idVendedorLogueado, contador, nombreProducto, codigoProducto, productosVendidos);
				numerar(nivel, idVendedorLogueado);
			} else {
				//*-llamada a la funcion cuando no hay datos y cargue bien el grafico ventas mensual
				numerar(nivel, idVendedorLogueado);
				graficoProductosMasVendidos();
				bar_chartRentabilidad();
				bar_RentabilidadNotaPedido();
			}

		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});
}
//*-productos vendidos---------------------------------------------------------------------------------------------
function ProductosMasVendidos(fecha_1, fecha_2, nivel, idVendedorLogueado, contador, nombreProducto, codigoProducto, productosVendidos) {

	var sql = 'SELECT nombreProducto,count(*) as productos from ' +
		' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta ' +
		' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" and vr.codigoProducto =' + codigoProducto[contador] + ' ';



	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },
		beforeSend: function (objeto) {
			$("#salidaTabla").html("<img src='imagenes/ajax-loader.gif'>");
		},
		success: function (data) {

			contador++;
			var arreglo = JSON.parse(data);

			for (var i = 0; i < arreglo.length; i++) {
				productosVendidos.push(arreglo[i]['productos']);
				nombreProducto.push(arreglo[i]['nombreProducto']);
			}

			NOMBREPRODUCTO=nombreProducto;
			if (contador == codigoProducto.length) {
				if(productosVendidos.length > 0){
					cantidadDeProductosVendidos(fecha_1, fecha_2, nivel, idVendedorLogueado);

				

					graficoProductosMasVendidos(nombreProducto.slice(0, 7), productosVendidos);

				}	else{
					graficoProductosMasVendidos();
				}
			} else {
				ProductosMasVendidos(fecha_1, fecha_2, nivel, idVendedorLogueado, contador, nombreProducto, codigoProducto, productosVendidos);
			}

		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});


}
//*Calcular los meses y sus dias---------------------------------------------
function fechaPorDia(año, dia) {
	var date = new Date(año, 0);
	return new Date(date.setDate(dia));
}
//*-numerar los meses y sus dias
function numerar(nivel, idVendedorLogueado) {
	var arrayMeses = [];
	//*-dias del mes
	var arrayDias = [];
	var meses = new Array(12);
	meses[0] = "ene";
	meses[1] = "febr";
	meses[2] = "mar";
	meses[3] = "abr";
	meses[4] = "may";
	meses[5] = "jun";
	meses[6] = "jul";
	meses[7] = "agos";
	meses[8] = "sep";
	meses[9] = "oct";
	meses[10] = "nov";
	meses[11] = "dec";

	for (i = 1; i < 366; i++) {
		let fecha = fechaPorDia(YEARS, i);

		let mes = fecha.getMonth();
		let dia = fecha.getDate();
		let dia_semana = fecha.getDay();
		if (fecha.getMonth() == MES) {

			arrayMeses.push(meses[mes] + dia);
			arrayDias.push(dia);

		}
	}
	ventasDiaPorMes(arrayMeses, arrayDias, nivel, idVendedorLogueado);

}
//*-funcion que trae las ventas por dia
function ventasDiaPorMes(arrayMeses, arrayDias, nivel, idVendedorLogueado) {

	var contador = 0;
	var utilidadProductos = [];

	for (var i = 0; i < arrayDias.length; i++) {
		utilidadProductos[i] = 0;
	}

	for (var i = 0; i < arrayDias.length; i++) {

		if (nivel > 0) {
			var mes = parseInt(MES) + 1;
			var filtroVendedor = 'AND v.id_vendedor=' + idVendedorLogueado;
			var sql = 'select DISTINCT v.total ,DATE_FORMAT(v.fechaNotaPedido, "%d") as dia from ' +
				'  ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta where month(fechaNotaPedido)=' + mes + ' and DAY(fechaNotaPedido)=' + arrayDias[i] + ' and YEAR(fechaNotaPedido)=2018 ' + filtroVendedor;
		}
		else {
			var mes = parseInt(MES) + 1;
			var sql = 'select DISTINCT v.total ,DATE_FORMAT(v.fechaNotaPedido, "%d") as dia from ' +
				'  ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta where month(fechaNotaPedido)=' + mes + ' and DAY(fechaNotaPedido)=' + arrayDias[i] + ' and YEAR(fechaNotaPedido)=2018 ';
		}


		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },
			beforeSend: function (objeto) {
				$("#salidaTabla").html("<img src='imagenes/ajax-loader.gif'>");
			},
			success: function (data) {
				if (data < 0) {
					return;
				} else {
					contador++;
					var arreglo = JSON.parse(data);
					var sumaUtilidad = 0;
					for (var i = 0; i < arreglo.length; i++) {
						sumaUtilidad += parseInt(arreglo[i]["total"]);
						//utilidadProductos.push(sumaUtilidad);
						utilidadProductos[parseInt(arreglo[i]["dia"] - 1)] = sumaUtilidad;

					}

					if (contador == arrayDias.length) {
					var arrayUtilidadPhp = utilidadProductos.toString();
							ARRAYUTILIDAD=arrayUtilidadPhp;
							ARRAYDIAMES=arrayMeses;

						areaVentasMensual(arrayMeses, utilidadProductos);

					}
				}

			},
			error: function (request, status, error) {
				console.error('Error: Could not documento');
			}
		});

	}
}
//*-----cantidad productos vendidos
function cantidadDeProductosVendidos(fecha_1, fecha_2, nivel, idVendedorLogueado) {
	var codigoProducto = [];
	var cantidadProductos = [];
	var arrayTotalUnitario = [];
	var arrayNombreProductos = [];
	var contador = 0;

	if (nivel > 0) {
		var filtroVendedor = 'AND v.id_vendedor=' + idVendedorLogueado;
		var sql = 'select DISTINCT vr.codigoProducto from ' +
			' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta  ' +
			' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"' + filtroVendedor;
	} else {
		var sql = 'select DISTINCT vr.codigoProducto from ' +
			' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta  ' +
			' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"';
	}

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var arreglo = JSON.parse(data);
			for (var i = 0; i < arreglo.length; i++) {
				codigoProducto.push(arreglo[i]['codigoProducto']);
			}
			if (codigoProducto.length > 0) {
				renta(fecha_1, fecha_2, codigoProducto, contador, cantidadProductos, arrayTotalUnitario, arrayNombreProductos, nivel, idVendedorLogueado);
			} else {
				bar_chartRentabilidad();
				bar_RentabilidadNotaPedido();

			}

		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});
}
//*-rentabilidad cinco pruductos mas vendidos
function renta(fecha_1, fecha_2, codigoProducto, contador, cantidadProductos, arrayTotalUnitario, arrayNombreProductos, nivel, idVendedorLogueado) {
	var arrayPrecioCompraMax = [];

	if (nivel > 0) {

		var filtroVendedor = 'AND v.id_vendedor=' + idVendedorLogueado;
		var sql = 'select vr.nombreProducto,vr.codigoProducto, vr.cantidad,vr.totalUnitario from ' +
			' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta  ' +
			' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" and vr.codigoProducto=' + codigoProducto[contador] + '  ' + filtroVendedor;
	} else {
		var sql = 'select vr.nombreProducto,vr.codigoProducto, vr.cantidad,vr.totalUnitario from ' +
			' ventas_relacional vr inner join ventas v on v.numeroCotizacion=vr.idVenta  ' +
			' where v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" and vr.codigoProducto=' + codigoProducto[contador];
	}
	
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			contador++
			var arreglo = JSON.parse(data);

			var sumaCantidadProd = 0;
			var sumaTotalUnitario = 0;
			var nombrePro;
			for (var i = 0; i < arreglo.length; i++) {
				sumaCantidadProd += parseInt(arreglo[i]["cantidad"]);
				sumaTotalUnitario += parseInt(arreglo[i]["totalUnitario"]);
				nombrePro = arreglo[i]['nombreProducto'];
			}

			cantidadProductos.push(sumaCantidadProd);
			arrayTotalUnitario.push(sumaTotalUnitario);
			arrayNombreProductos.push(nombrePro);
			CANTIDADPRODUCTOS = cantidadProductos;
			NOMMAYORRENT = arrayNombreProductos;		
		
			if (contador == codigoProducto.length) {
				var arrayDivicionPrecioTotalUni = [];
				for (i = 0; i < arrayTotalUnitario.length; i++) {
					arrayDivicionPrecioTotalUni.push(parseInt(arrayTotalUnitario[i]) / parseInt(cantidadProductos[i]));
				}

				var contadorPrecioMax = 0;
				consultarPrecioMax(codigoProducto, contadorPrecioMax, arrayPrecioCompraMax, arrayDivicionPrecioTotalUni,nivel);
				consultasVentas(fecha_1, fecha_2, nivel, idVendedorLogueado);
				graficoProductosMasVendidos(NOMMAYORRENT.slice(0, 5), CANTIDADPRODUCTOS);		
			} else {
				renta(fecha_1, fecha_2, codigoProducto, contador, cantidadProductos, arrayTotalUnitario, arrayNombreProductos, nivel, idVendedorLogueado);
			}

		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});
}
//*-consultar el precio maximo
function consultarPrecioMax(codigoProducto, contadorPrecioMax, arrayPrecioCompraMax, arrayDivicionPrecioTotalUni,nivel) {

	var sql = 'Select precio_compra_max from productos where codigo=' + codigoProducto[contadorPrecioMax];

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			contadorPrecioMax++

			var arreglo = JSON.parse(data);
			for (var i = 0; i < arreglo.length; i++) {
				arrayPrecioCompraMax.push(arreglo[i]["precio_compra_max"]);
			}

			if (contadorPrecioMax == codigoProducto.length) {
				var arrayGananciaUnitaria = [];
				var arrayGananciaTotal = [];

				for (i = 0; i < arrayPrecioCompraMax.length; i++) {
					arrayGananciaUnitaria.push(parseInt(arrayDivicionPrecioTotalUni[i]) - parseInt(arrayPrecioCompraMax[i]));
				}
				for (i = 0; i < CANTIDADPRODUCTOS.length; i++) {
					arrayGananciaTotal.push(parseInt(arrayGananciaUnitaria[i]) * parseInt(CANTIDADPRODUCTOS[i]));
				}
				bar_chartRentabilidad(NOMMAYORRENT.slice(0, 10), arrayGananciaTotal);

				var arrayGananciaPhp = arrayGananciaTotal.toString();
				ARRAYGANANCIA = arrayGananciaPhp;
				guardar_grafico1();

			} else {
				consultarPrecioMax(codigoProducto, contadorPrecioMax, arrayPrecioCompraMax, arrayDivicionPrecioTotalUni);
			}

		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});

}
//*-consultas ventas neto
function consultasVentas(fecha_1, fecha_2, nivel, idVendedorLogueado) {

	var arraySumaVentas = [];
	if (nivel > 0) {
		var filtroVendedor = 'AND id_vendedor=' + idVendedorLogueado;
		var sql = 'select neto from ventas where  fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"  ' + filtroVendedor;
	} else {
		var sql = 'select neto from ventas where  fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"';
	}


	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var sumasVentas = 0;
			var promedioVenta = 0;
			var largoArrayVenta;
			var arreglo = JSON.parse(data);

			largoArrayVenta = arreglo.length;
			for (var i = 0; i < arreglo.length; i++) {
				sumasVentas += parseInt(arreglo[i]["neto"]);
			}
			arraySumaVentas.push(sumasVentas);
			promedioVenta = sumasVentas / largoArrayVenta;

			calcularGanaciaPromedio(fecha_1, fecha_2, sumasVentas, largoArrayVenta, promedioVenta, nivel, idVendedorLogueado);



		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});

}

function calcularGanaciaPromedio(fecha_1, fecha_2, sumasVentas, largoArrayVenta, promedioVenta, nivel, idVendedorLogueado) {
	var arrayCantidad = [];
	var arrayPrecioCompraM = [];
	var arrayTotalCosto = [];

	if (nivel > 0) {
		var filtroVendedor = 'AND v.id_vendedor=' + idVendedorLogueado;
		var sql = 'select vr.codigoProducto,vr.cantidad,p.precio_compra_max from ' +
			' ventas v inner join ventas_relacional vr on vr.idVenta=v.numeroCotizacion ' +
			' join productos p on p.codigo=vr.codigoProducto where  v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59" ' + filtroVendedor;
	} else {
		var sql = 'select vr.codigoProducto,vr.cantidad,p.precio_compra_max from ' +
			' ventas v inner join ventas_relacional vr on vr.idVenta=v.numeroCotizacion ' +
			' join productos p on p.codigo=vr.codigoProducto where  v.fechaNotaPedido between "' + fecha_1 + ' 00:00:00" and "' + fecha_2 + ' 23:59:59"';

	}


	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: { sql: sql, tag: 'array_de_datos' },

		success: function (data) {
			var arreglo = JSON.parse(data);
			var sumaProductoConsto = 0;
			var ganancia = 0;
			var promedioGanancia = 0;
			for (var i = 0; i < arreglo.length; i++) {
				arrayPrecioCompraM.push(arreglo[i]["precio_compra_max"]);
				arrayCantidad.push(arreglo[i]["cantidad"]);
			}
			for (var i = 0; i < arrayPrecioCompraM.length; i++) {
				arrayTotalCosto.push(parseInt(arrayCantidad[i]) * parseInt(arrayPrecioCompraM[i]));
			}
			for (var i = 0; i < arrayTotalCosto.length; i++) {
				sumaProductoConsto += arrayTotalCosto[i];
			}

			ganancia = parseInt(sumasVentas) - parseInt(sumaProductoConsto);

			promedioGanancia = parseInt(ganancia) / parseInt(largoArrayVenta);

			bar_RentabilidadNotaPedido(redondeo(promedioVenta, 0), redondeo(promedioGanancia, 0));

			PROMEDIOVENTA = redondeo(promedioVenta, 0);
			PROMEDIOGANANCIA = redondeo(promedioGanancia, 0);


		},
		error: function (request, status, error) {
			console.error('Error: Could not documento');
		}
	});

}

//*-------------------------------------------------
