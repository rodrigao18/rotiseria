
	//CARGAR LAS CATEGORIAS EN LA TABLA PM 14:06
function cargar_categorias(){
	//SQL SELECT;
	var sql ='select id, nombre_categoria,descripcion,alias from categorias';
	console.log('sql: ', sql);	
	//AJAX
	$.ajax({
	type: 'POST',
	url: 'php/consulta.php',
	data: {	sql:sql,tag: 'array_de_datos'},
	success:function (data) {				
				var arreglo = JSON.parse(data);
				console.log(arreglo);
				mostrar_formulario(arreglo);
	},
  error:function (request, status, error) {
 				alert("Error: Could not cargado");
 	}
 });
}
  //CARGAR TABLA CATEGORIAS;
function mostrar_formulario(arreglo){
	$("#salida").append(
		'<table class="table table-striped " id="tablaProductosDescartados">' +
		'<thead>' +
		'<tr>' +
		'<th scope="col">Código</th>' +		
		'<th scope="col">Nombre</th>' +
		'<th scope="col">Descripción</th>'+
		'<th scope="col">Alias</th>' +
    '<th></th>' +
    '<th></th>' +
		'</tr>' +
		'</thead>' +
		'<tbody id="tablaBody"></tbody></table>');

		for (var i = 0; i < arreglo.length; i++) {
		var id = arreglo[i][0];
		var nombre = arreglo[i][1];	
    var descripcion = arreglo[i][2];
    var alias = arreglo[i][3];		
		
	$("#tablaBody").append('<tr>' +
		'<td width="10%">' + id + '</td>' +	
		'<td width="10%">' + nombre + '</td>' +
		'<td width="20%">' + descripcion + '</td>' +
		'<td width="20%">' + alias + '</td>' +			
		'<td width="10%"><form method="POST" action="editar_categoria.php">' +
		'<button type="submit" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Editar" name="id" value="' + id + '" ><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>' +
		'<td width="10%"><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminar_categoria(event,' + id + ')><i class="fas fa-trash" aria-hidden="true"></i></button></td>' +
		'</tr>');
	}
	$('[data-toggle="tooltip"]').tooltip();
	//lenguaje();
}
  //ELIMINAR UNA CATEGORIA;
function eliminar_categoria(e,id){
	e.preventDefault();
	swal({
		title: "Borrar categoria",
		text: "¿ Esta seguro de eliminar esta categoria ?",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			//SQL DELETE;
			var sql ='DELETE from categorias where id=' + id;
			console.log(sql);
			//AJAX;
			$.ajax({
			type: 'POST',
			url:  'php/consulta.php',
			data: {sql:sql,tag: 'crud_productos'},
			success: function (data) {
			if(data==1){
				swal("Delete!", "categoria elminada!", "success");
				window.location.href="ver_categorias.php";
			} 
			},
			error:function (request, status, error) {
			console.error("Error: Could not borrado");
				}
				});
				} else {
				return;
				}
			});


}

window.onload = cargar_categorias