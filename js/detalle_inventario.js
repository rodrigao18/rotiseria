function cargarDatosIniciales() {
	
	var sql = 'SELECT ir.codigo_barra,nombre,cantidad_ingresada,stock from inventario_relacional ir inner join productos p on p.codigo_barra=ir.codigo_barra where id_inventario =' + NUMEROINVENTARIO;

	console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arregloInvent = JSON.parse(data);
			console.error(arregloInvent);
			document.getElementById('numInv').innerHTML = NUMEROINVENTARIO;
			vendedor();
			addProducto(arregloInvent);
		},
		error: function (request, status, error) {
			console.error("Error: Could not cargarCotizaciones");
		}
	});
}
//*-cargar vendedor
function vendedor() {
	var sql = 'select nombreVendedor from vendedores where id_vendedor =' + ID_VENDEDORLOGUEADO;
	console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			document.getElementById('vendedor').value = arreglo[0]['nombreVendedor'];
		},
		error: function (request, status, error) {
			console.error("Error: Could not cargarCotizaciones");
		}
	});

}

//*-agrego los productos a una tabla detalles;
function addProducto(arreglo) {    
   
    var stockAnterior = 0;  
	
	for (var i = 0; i < arreglo.length; i++) {

		var stockAnterior = arreglo[i]['stock'] - arreglo[i]['cantidad_ingresada'];
		
    $("#tablaBodyVentas").append('<tr id="fila">' +
    '<td></td>' +
    '<td>' + arreglo[i]['codigo_barra'] + '</td>' +
    '<td>' + arreglo[i]['nombre'] + '</td>' +
    '<td>' + stockAnterior + '</td>' +
    '<td>'+arreglo[i]['cantidad_ingresada'] +'</td>' +
    '<td>' + arreglo[i]['stock'] + '</td>' +    
	'</tr>');
		
	}	
    agregarNumeracionItem();
	lenguaje();	
}

//FUNCTION PARA AGREGAR UN ITEM A LA VENTA NUEVA Y RESETEAR CUANDO SE BORRE UN ITEM;
function agregarNumeracionItem() {
	var tabla = document.getElementById("tablaBodyVentas"),
		rIndex;
	var nFilas = $("#tablaBodyVentas > tr").length;
	for (var i = 0; i < nFilas; i++) {
		tabla.rows[i].cells[0].innerHTML = i + 1;
	}

}

function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tabla_detalle_inventario').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'Detalle de inventario ' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'Detalle de inventario ' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}