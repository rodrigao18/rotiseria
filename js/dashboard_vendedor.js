function pie_chart(valor1, valor2) {
  var pieChartContent = document.getElementById('grafico');
  pieChartContent.innerHTML = '&nbsp;';

  $('#grafico').append('<canvas id="pieChartDemo"><canvas>');

  data = {
    datasets: [{
      data: [valor1, valor2],
      backgroundColor: ["#2c80b9", "#ddeaf6"]

    }],
    labels: [
      '% Porcentaje de captura',
      '% No capturado',
    ]

  };

  var ctxp = $("#pieChartDemo").get(0).getContext("2d");
  var pieChart = new Chart(ctxp, {
    type: 'pie',
    data: data,
    options: {
      tooltips: {
        // Disable the on-canvas tooltip
        enabled: true,

      }
    }
  });

}
//*-graficos rentabilidad promedio nota de pedido 
function graficoVendedores(arrayCotizacion, arrayNotaPedido) {

  var pieChartContent = document.getElementById('graficoBarras');
  pieChartContent.innerHTML = '&nbsp;';
  $('#graficoBarras').append('<canvas id="barChartDemo"><canvas>');
  //*-colores
  arrayColores = ["#ffc14b", "#009588"];
  arrayNombre = ["Cotizaciones", "Nota de pedidos"];
  datos = [arrayCotizacion, arrayNotaPedido];
  var data = {
    labels: arrayNombre,
    color: "#c6e3b1",
    datasets: [{
      label: "Valores reales",
      data: datos,
      backgroundColor: arrayColores,
    }, ],
  };

  var ctxb = $("#barChartDemo").get(0).getContext("2d");

  var barChart = new Chart(ctxb, {
    type: 'bar',
    data: data,
    options: {
      scales: {
        xAxes: [{
          stacked: true,
          ticks: {
            min: 0,

          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {}
        }]
      },
    }
  });
}

//*-grafico flujo de cajas gastos generales

function graficoGastosgenerales(matriz, categorias) {

  var pieChartContent = document.getElementById('graficoBarrasGastosGenerales');
  pieChartContent.innerHTML = '&nbsp;';
  $('#graficoBarrasGastosGenerales').append('<canvas id="barChartGastos"><canvas>');


  var data = {
    labels: categorias,
    datasets: [{
        label: "Enero",
        data: matriz[0],
        backgroundColor: "#006AA8",

      },
      {
        label: "Febrero",
        data: matriz[1],
        backgroundColor: "#FF0060"
      },
      {
        label: "marzo",
        data: matriz[2],
        backgroundColor: "#1AC436"
      },
      {
        label: "abril",
        data: matriz[3],
        backgroundColor: "#949494"
      },
      {
        label: "mayo",
        data: matriz[4],
        backgroundColor: "#f45a2b"
      },
      {
        label: "junio",
        data: matriz[5],
        backgroundColor: "#16a9df"
      },
      {
        label: "julio",
        data: matriz[6],
        backgroundColor: "#387F71"
      },
      {
        label: "agosto",
        data: matriz[7],
        backgroundColor: "#f4d03f"

      },
      {
        label: "septiembre",
        data: matriz[8],
        backgroundColor: "#1CCCAA"
      },
      {
        label: "octubre",
        data: matriz[9],
        backgroundColor: "#4a235a"
      },
      {
        label: "noviembre",
        data: matriz[10],
        backgroundColor: "#3498db"
      },
      {
        label: "diciembre",
        data: matriz[11],
        backgroundColor: "#fad7a0"
      }
    ]
  };

  var ctxb = $("#barChartGastos").get(0).getContext("2d");
  // var barChart = new Chart(ctxb).Bar(data);

  var barChart = new Chart(ctxb, {
    type: 'bar',
    data: data,
    options: {
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      },

    }

  });

}