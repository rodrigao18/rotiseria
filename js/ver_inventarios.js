//*------------------------------------------------
function cargarInventarios(fecha_inicio) {
	console.log(fecha_inicio);
	var sql='select i.id,fecha_inventario,codigo_barra from inventario i inner join inventario_relacional ir on ir.id_inventario=i.id group by id ';
	$("#salida").html("").fadeIn('slow');

	console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			console.log(arreglo);
			
			//mostrarFormulario(arreglo);
			consultaProductosInventario(arreglo)
			
		},
		error: function (request, status, error) {
			console.error("Error: Could not cargarCotizaciones");
		}
	});
}

function consultaProductosInventario(arreglo) {
	
	for (var i = 0; i < arreglo.length; i++) {
		
		var sql = 'select i.id,fecha_inventario,count(codigo_barra) as productos,sum(cantidad_ingresada) as cantIngresada , estado from '+
		'inventario i inner join inventario_relacional ir on ir.id_inventario = i.id where i.id = '+ arreglo[i]['id'] + ' order by fecha_inventario asc';
		
		console.log(sql);
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {
				tag: 'array_de_datos',
				sql: sql
			},
			success: function (data) {
				var arregloProductos = JSON.parse(data);
				console.log(arregloProductos);			
				mostrarFormulario(arregloProductos);				
			
			},
			error: function (request, status, error) {
				console.error("Error: Could not cargarCotizaciones");
			}
		});
	}

	 }

function mostrarFormulario(arreglo) {
	for (var i = 0; i < arreglo.length; i++) {

		var id = arreglo[i]['id'];
		var fecha = arreglo[i]['fecha_inventario'];
		var productos = arreglo[i]['productos'];		
		var estadoInventario = arreglo[i]['estado'];
		var cantiIngresada = arreglo[i]['cantIngresada'];
		var EstadoColumna;	
		if (estadoInventario == 1) {
			EstadoColumna = "<span class='badge badge-success'>Inventario ok</span>";
		}
	
		$("#tablaBody").append('<tr>' +
			'<th>' + id + '</th>' +
			'<td>' + fecha + '</td>' +
			'<td>' + productos + '</td>' +
			'<td>' + cantiIngresada + '</td>' +
			'<td>' + EstadoColumna + '</td>' +
			'<td><form method="POST" action="detalle_inventario.php">' +
			'<input type="hidden" class="form-control" id="numeroInventario" name="numeroInventario" value="' + id + '">' +			
			'<input type="hidden" class="form-control" id="estadoVenta" name="estadoVenta" value="' + estadoInventario + '">' +
			'<button type="submit" class="btn btn-mini" data-toggle="tooltip" data-placement="top" title="Detalles"><i class="fa fa-th-list" aria-hidden="true"></i></button></form></td>' +
			'</tr>');

	}
	$('[data-toggle="tooltip"]').tooltip();
	//lenguaje();
}
function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaVentas').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]]
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}