//CARGAR CATEGORIA
 function cargar_categoria() {

	var sql = 'SELECT id,nombre_categoria FROM categoria';
	//AJAX	
	$.ajax({
	type: 'POST',
	url:  'php/consulta.php', 
	data: {sql: sql,tag: 'categoria'},
			
	success:function (data){
				$('#select_categoria').html(data).fadeIn();
			
	},
	error: function (request, status, error)
				{alert('Error: Could not categoria');
	}
	})

}
window.onload = cargar_categoria
/*-----------------------------------------------*/

/*----------------------------Agregar productos------------------------*/
//*-guardar productos;	
function GuardarProducto(e) {
	e.preventDefault();
	var nombre = $("#nombreProducto").val();
	var stock = $("#stock").val();
	var stockMinimo = $("#stockMinimo").val();
	var codigoProducto = $("#codigoProducto").val();	
	var idCategoria = document.getElementById("select_categoria").value;
	var precioCompra = $("#precioCompra").val();
	var precioVenta = $("#precioVenta").val();
	var kilos = document.getElementById('kg').value;

	//SQL INSERT;
	var sql = 'insert into productos (codigo_barra,nombre, precio_compra, precio_venta, stock, stock_minimo,id_categoria,kilogramos)' +
		'VALUES(' + codigoProducto + ',"' + nombre + '",' + precioCompra + ',' + precioVenta + ',' + stock + ',' + stockMinimo + ',' + idCategoria + ','+ kilos +')';
	

	//COMPROBAR LOS CAMPOS QUE NO ESTEN VACÍOS;
	if ($("#nombreProducto").val() == "" || $("#stockMinimo").val() == "" ||
		$("#precioCompra").val() == 0 || $("#precioCompra").val() == "" ||
		$("#precioVenta").val() == 0 || $("#precioVenta").val() == "") {
		swal("Debe Llenar los Campos!", "", "info");
		//swal("Faltan datos", "Debe completar los campos para continuar", "info");

	} else{
		//AJAX;
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			async: true,
			data: {
				tag: 'crud_productos',
				sql: sql
			},
			success: function (data) {
				console.info(data);
				if (!isNaN(data)) {
					swal("Insert!", "El producto fue ingresado correctamente!", "success");				
					window.location.href = "ver_productos.php"
				} else {
					alert('Error En El Ingreso');
				}
			},
			error: function (request, status, error) {
				alert("Error: Could not  guardarProducto");
			}
		});
	}	
}




