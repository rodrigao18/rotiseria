var editando = false;
var MARGEN;
//funcion para tranformar una celda editable
function transformarEnEditable(nodo,cantTicket) {

	//El nodo recibido es SPAN

	if (editando == false) {	
		
		var nodoTd = nodo.parentNode; //Nodo TD
		var nodoTr = nodoTd.parentNode; //Nodo TR
		var nodosEnTr = nodoTr.getElementsByTagName('td');
		var cantidad = convertirNumeros(nodoTd.textContent);
		var nuevoCodigoHtml= '<td><input type="number" id="nombre" value=' + cantidad + ' width="100%" onkeypress="pulsar(event,this,'+cantTicket+')" onkeyup="salir(event,this)"></td>';	
		
		nodoTd.innerHTML = nuevoCodigoHtml;
		editando = "true";
	} else {
		alert('Solo se puede editar una línea. Recargue la página para poder editar otra');
	}


}

function pulsar(e, nodo,cantTicket) {

	if (e.keyCode === 13 && !e.shiftKey) {
		e.preventDefault();

		var nodoTd = nodo.parentNode; //Nodo TD
		var nodoTr = nodoTd.parentNode; //Nodo TR		
		var nodosEnTr = nodoTr.getElementsByTagName('td');		
		var precio = nodosEnTr[5].textContent;
		console.log('precio unitario ' + precio);
		console.error('cantTicket ' +cantTicket);
	
		/*----------------------------------------------------------*/
		
		var cantidad = document.getElementById('nombre').value;
		//*-valor total cantidad por el precio	
		var total = convertirNumeros(cantidad) * convertirNumeros(precio);
		var totalMostrar = new Intl.NumberFormat('es-MX').format(Math.round(total));

		//*-Paso el valor calculado de precioUnitario por la cantidad a la columna total 
		//*-acceso al nodo hijo del padre TR con childNodes
		nodoTr.children[6].innerHTML=totalMostrar;
		document.getElementById('colsCantidad'+cantTicket).innerHTML=cantidad;
		document.getElementById('colsPrecio'+cantTicket).innerHTML=total;
		var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarEnEditable(this,'+cantTicket+')" style="cursor:pointer;"> ' + cantidad + '</span> </td>';	
		
	/*-------------------------------------------------------------------------------------*/	
		
	/*-------------------------------------------------------------------------------------*/	
		nodoTd.innerHTML = nuevoCodigoHtml;

		editando = false;
		calcularValores();
	}
}
//*-salir de la celda editable si no deseo editar
function salir(event, nodo){
  if (event.code === 'Escape' || event.keyCode === 27) {
	console.log('escape');
	event.preventDefault();

	var nodoTd = nodo.parentNode; //Nodo TD
	var nodoTr = nodoTd.parentNode; //Nodo TR		
	var nodosEnTr = nodoTr.getElementsByTagName('td');		
	var precio = nodosEnTr[4].textContent;
	
		/*----------------------------------------------------------*/

	var cantidad = document.getElementById('nombre').value;

	//*-valor total cantidad por el precio	

		//*-Paso el valor calculado de precioUnitario por la cantidad a la columna total 
		//*-acceso al nodo hijo del padre TR con childNodes
	
		
		var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarEnEditable(this)"> ' + cantidad + '</span> </td>';		

		nodoTd.innerHTML = nuevoCodigoHtml;

		editando = false;
	  }
}



