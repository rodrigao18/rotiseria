var ID;

/*-------------------------------Cargar los datos del producto-----------------*/
//LLAMADA  AKA BASE PARA TRAER LOS DATOS Y CARGARLO EN EL FORMULARIO;
function cargar_datos_formulario(id) {
	ID = id;
	console.log(ID);
	var sql = 'SELECT id,codigo_barra,nombre, precio_compra, precio_venta, stock, stock_minimo,id_categoria,kilogramos FROM productos WHERE id=' + ID;
	console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {	sql: sql, tag: 'array_de_datos'	},
		success: function (data) {
			var arreglo = JSON.parse(data);
			console.log(arreglo);
			formulario_editar(arreglo);
			var id_categoria = arreglo[0]["id_categoria"];
			cargarCategoria(id_categoria);
		
		},
		error: function (request, status, error) {
			alert("Error: Could not consultarProducto");
		}
	});

}
  //CARGAR CATEGORIAS;
	function cargarCategoria(id){
		//SQL SELECT;
		var sql = 'SELECT id, nombre_categoria FROM categoria';
		console.log(sql);
		//AJAX;
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {sql: sql,tag: 'categoria'},
					 
			success: function (data){    
						$('#select_categoria').html(data).fadeIn();
		//DEJAR SELECCIONADO EL PROVEEDOR CON EL ID SELECCIONADO SACADO DESDE LA FUNCION LLAMADA A LA BASE;  
						$('#select_categoria option[value="' + id + '"]').attr("selected", true);   
			},
			error:function (request, status, error)
						{alert('Error: Could not categoria');}
			})
	}
//FORMULARIO PARA EDITAR
function formulario_editar(arreglo) {

	var codigo = arreglo[0]['codigo_barra'];
	var nombreProducto = arreglo[0]['nombre'];
	var stock = arreglo[0]['stock'];
	var stockMinimo = arreglo[0]['stock_minimo'];	
	var precioCompra = arreglo[0]['precio_compra']	
	var precioVenta = arreglo[0]['precio_venta'];	
	var kilo=arreglo[0]['kilogramos']


	//NAMES DE LOS INPUT
	$("#codigoProducto").val(codigo);
	$("#nombreProducto").val(nombreProducto);
	$("#stock").val(stock);
	$("#stockMinimo").val(stockMinimo);
	$("#precioCompra").val(precioCompra);
	$("#precioVenta").val(precioVenta);
	$("#kg").val(kilo);
}
/*-----------------------------------------------------------------------------*/


//FUNCION AJAX PARA REALIZAR EL UPDATE;
function EditarProducto(e) {
	e.preventDefault();

	var codigo = $("#codigoProducto").val();
	var nombre_producto = $("#nombreProducto").val();
	var stock = $("#stock").val();
	var stock_minimo 		= $("#stockMinimo").val();	
	var precio_compra 	= $("#precioCompra").val();	
	var precio_venta 		= $("#precioVenta").val();	
	var categoria = document.getElementById("select_categoria").value;
	var kilog = document.getElementById('kg').value;
	//SQL UPDATE;
	var sql = 'UPDATE productos set codigo_barra = "' + codigo + '" ,nombre = "' + nombre_producto + '" , ' +
		'	precio_compra = ' + precio_compra + ' , precio_venta = ' + precio_venta + ',' +
		' stock = ' + stock + ', stock_minimo = ' + stock_minimo + ', id_categoria = ' + categoria + ' , kilogramos = ' + kilog + ' ' +
		'	where id = ' + ID + ';'
			console.log(sql);

	//COMPROBAR LOS CAMPOS QUE NO ESTEN VACÍOS;
	if ($("#nombreProducto").val() == "" ||
		$("#precioVenta").val() == 0 || $("#precioVenta").val() == "") {

		swal("Faltan datos", "Debe completar los campos para continuar", "info");

	}  else {
		//AJAX	
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {
				sql: sql, tag: 'crud_productos' },
			success: function (data) {
				console.log(data);
				if (data == 1) {
				
					swal("Update!", "Datos actualizados correctamente!", "success");
					window.location.href = "ver_productos.php"
				} else {
					alert('Error En El Ingreso');
				}
			},
			error: function (request, status, error) {
				alert("Error: Could not editarProducto");
			}
		});
	}
}
