var VENDEDOR;
var ACTIVO=true;
let cargar_ventaInicio_onchange = (NIVEL,ID_VENDEDORLOGUEADO)=>{
	$("#tablaBody").html("").fadeIn('slow');
	ACTIVO=false;
	if(ACTIVO==true){
	lenguaje();
	return;
	}
	
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_termino = $("#fecha_termino").val();
	cargarVentas(fecha_termino,fecha_inicio);

}


function cargarVentas(fecha_actual,fecha_termino){	

	if (NIVEL > 0) {
		var filtroVendedor = 'AND id_vendedor=' + ID_VENDEDORLOGUEADO;
		var sql=`SELECT numeroVenta,id_vendedor,DATE(fechaVenta) as fecha,TIME(fechaVenta) as hora ,totalVenta 
		from ventas where fechaVenta BETWEEN '${fecha_termino} 00:00:00' AND  '${fecha_actual} 23:58:59'  ${filtroVendedor} ORDER BY numeroVenta DESC`;

	}else{
		var sql=`SELECT numeroVenta,id_vendedor,DATE(fechaVenta) as fecha,TIME(fechaVenta) as hora ,totalVenta 
		from ventas where fechaVenta BETWEEN '${fecha_termino} 00:00:00' AND  '${fecha_actual} 23:58:59' ORDER BY numeroVenta DESC`;
	}

	
	
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			
			
			tablaVentas(arreglo);
			
		},
		error: function (request, status, error) {
			console.error("Error: Could not cargarCotizaciones");
		}
	});


}


function tablaVentas(arreglo){

	for (var i = 0; i < arreglo.length; i++) {
		$("#tablaBody").append(
			`<tr>		   
			<td>${arreglo[i]['numeroVenta']}</td>
			<td>${VENDEDOR[`${arreglo[i]['id_vendedor']}` - 1 ]["nombreVendedor"]}</td>
			<td>${arreglo[i]['fecha']}</td>
			<td>${arreglo[i]['hora']}</td>		  
			<td>${arreglo[i]['totalVenta']}</td>		  
			<td><form method="POST" action="detalle_venta.php">
			<input type="hidden" class="form-control" id="id" name="id" value="1">
			<input type="hidden" class="form-control" id="numeroVenta" name="numeroVenta" value=${arreglo[i]['numeroVenta']}>
			<button type="submit" class="btn btn-secondary" data-toggle="tooltip"		 
			 data-placement="top" title="Editar" name="id" value=${arreglo[i]['numeroVenta']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>
			 <td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${arreglo[i]['numeroVenta']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		  </tr>`
		);

	}
	$('[data-toggle="tooltip"]').tooltip();
	if(ACTIVO==true){
		lenguaje();
		}
}


 let consultarVendedor = async (fecha_actual,fecha_termino) => {

	
	const baseUrl = 'php/consultaFetch.php';
	let consulta = `SELECT nombreVendedor  
	FROM vendedores `;
	
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);
		console.log(array);
		VENDEDOR=array;
		cargarVentas(fecha_actual,fecha_termino);
	
		
	} catch (error) {
		console.log('error en la conexion en consultaVendedor ', error);
	}

  }


function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaVentas').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}

function eliminarProducto(e,id,index) {
	e.preventDefault();
	let mensaje;
	let titulo;
	if(index==1){
		titulo=`Eliminar venta`;
		mensaje=`¿esta seguro de eliminar la venta ?`;
	}

	swal({
		title: `Eliminar venta`,
		text: `¿esta seguro de eliminar la venta ?`,
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			eliminarVenta(id);
			
		} else {
			return;
		}

	});	

}

let eliminarVenta =async(idP)=>{

	const baseUrl = 'php/consultaFetch.php';

	let consulta=`DELETE FROM ventas WHERE numeroVenta=${idP}`;

	const sql   = {sql: consulta, tag: `crud`}	
	console.log(consulta);
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();		
		const borraVrelacional = borrVentaRe (idP);				
	} catch (error) { console.log('error en la conexion ', error); }

}

let borrVentaRe = async (idP) => {

	const baseUrl = 'php/consultaFetch.php';
	let consulta=`DELETE FROM VENTAS_RELACIONAL  WHERE idVenta=${idP}`;

	const sql   = {sql: consulta, tag: `crud`}	

	console.error(consulta);
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.		
			
		swal("Venta borrada", "los datos fueron borrados exitosamente", "success");
		setTimeout('location.reload()', 1000);
		
		
		
	} catch (error) { console.log('error en la conexion ', error); }


}
