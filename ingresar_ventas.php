<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistema View Point</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
    <!-- Font-icon css-->
    
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

</head>

<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-store"></i> Ventas </h1>
                <p>Ingresar ventas</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Ventas</li>
                <li class="breadcrumb-item active"><a href="#">Ingresar ventas</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                  <div class="float-right">
						<?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL,"es_ES");  echo "Fecha: ".strftime("%A %d de %B del %Y"); ?>
            <br>
						<?php echo "Hora: ".date("H:i:s"); ?>
            <br> <br>
					</div><br>
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                        <form method="POST" id="formularioGuardar">



                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fa fa-search" aria-hidden="true"></i></div>
                                </div>
                                <input type="text" class="form-control" id="buscarCodigoBarra" onkeyup="buscarProductos(event);" placeholder="Ingrese codigo de barras">
                            </div>


                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                        <form >

                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>                                                                                                           
                                        <th scope="col" width="10%"> Código</th>
                                        <th scope="col" width="30%"> Cantidad</th>
                                        <th scope="col" width="10%"> Descripción </th>                                       
                                        <th scope="col" width="15%"> Precio U.</th>
                                        <th scope="col" width="10%"> Desc </th>
                                        <th scope="col" width="15%"> Precio Total.</th>
                                        <th scope="col" width="10%">  </th>
                                        </tr>
                                    </thead>
                                <tbody id="tablaBodyVentas"></tbody>
                                </table>

                                <!-- TABLA TOTALNETO-->
                      					<table class="table table-striped" id="tablaTotal">
                      						<tbody id="valorTotal">
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Total </td>
                      								<td width="25%"><input type="text" class="form-control" id="total" disabled></td>
                      							</tr>
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Modo pago</td>
                      								<td width="25%"> <select class="form-control" id="selectModoPago" onchange="verificarMedioPago()">
                                              <option value="1"> EFECTIVO</option>
                                              <option value="2"> DEBITO</option>
                                              <option value="3"> CREDITO</option>
                                            </select></td>
                      							</tr>
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Monto Efectivo</td>
                      								<td width="25%"><input type="text" class="form-control" id="montoEfectivo" onkeyup="calcularVuelto()" ></td>
                      							</tr>

                                    <tr>
                                      <td colspan="3"></td>
                                      <td width="15%">Vuelto</td>
                                      <td width="25%"><input type="text" class="form-control" id="vuelto" disabled></td>
                                    </tr>
                      						</tbody>
                      					</table>                                        
							<br>
                        <!--Tabla ticket-->    
                        <div id ="conTicket">
    <table id="tablaTicketCabezera">
    <tbody>
        <tr>
			<td id="TituloTicketIngreso"></td>
		</tr>
        <tr>
			<td id="Posticket" ></td>
		</tr>
		<tr>
			<td id="fechaTicket" class="VendedorTicketClass" ></td>
		</tr>
        </tbody>
    </table>
    <table id="tablaTicket">
    <thead>    
        <tr>
			<th id="colNombre">Descripcion&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th>
            <th id="colCantidad">Cant.&nbsp&nbsp</th>
            <th></th>
            <th id="colPrecio">Precio</th>
		</tr>
    </thead>
	<tbody id="Filaticket" class="fiTicket">	
	    	
    </tbody>
         <tr>
			<th id="totalCompra"></td>
            <td></td>
            <td></td>
            <td id="totalCompra_precio"></td>
            <td></td>
		</tr>		
		<!-- <tr>
		<td id="printBarcode">
        <img id="barcode"/>
        </td>
        </tr> -->
       
</table>
<div id="printBarcode"> <img id="barcode"/></div>
<!-- Fin tabla ticket-->           
                            <br>
                            
							</div>
                            <button class="btn btn-primary float-right" onclick="finalizarVenta(event,'conTicket')"><i class="fa fa-save"></i> Finalizar venta </button>
                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>



<!-- Fin tabla ticket-->           
                            <br>
                            
							</div>
                            <button class="btn btn-primary float-right" onclick="finalizarVenta(event,'conTicket')"><i class="fa fa-save"></i> Finalizar venta </button>
                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Producto :  <span id="producto"></span></h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
       
        <div id="salidaModal">        
        <input type="hidden" class="form-control" id="codigoPromocion" >
        <input type="hidden" class="form-control" id="mostrarGramos">
        <input type="hidden" class="form-control" id="mostrarValorGramos">
        <input type="hidden" class="form-control" id="mostrarNombreProducto">
        </div>
        <div id="filaProductos">
            <div class="form-row">
                <div class="form-group col-md-6">
                <label>gramos</label>
                <input type="number" class="form-control" id="kg" min=0 name="kg" onclick="calcularPrecioKilos()" onkeyup="calcularPrecioKilos()" placeholder="Ingrese kilogramos" value=0>
                </div>
            
                <div class="form-group col-md-6">
                <label>Precio Kilo</label>
                <input type="text" class="form-control" id="precio" disabled name="precio"  value=0>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                <label>Valor</label>
                <input type="text" class="form-control" id="valorProducto" disabled name="valorProducto" placeholder="valor" value=0>
                </div>
            </div>       		
        </div>
	
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" disabled id="addPromocion"  class="btn btn-success" onclick=addmodalATabla(1)>Añadir</button>
        </div>
        
      </div>
    </div>
  </div> 
</div>

    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/ingresar_ventas.js?vp5"></script>
    <script type="text/javascript" src="js/editar.js"></script>
    <script type="text/javascript" src="js/funciones.js?vp5"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
    <script type="text/javascript" src="js/JsBarcode.all.min.js"></script>

    <script>
        <?php  $fecha  = date("Y-m-d") ?>;
        <?php $hora =   date("H:i:s") ?>;
        var FECHA = '<?php echo date("d-m-Y",strtotime($fecha)); ?>';
        var HORA = '<?php echo $hora ?>';
        var ID_VENDEDOR =<?php echo $idVendedor;?>; 
        var ID_TURNO = <?php echo $idTurno;?>;
        
        
		window.onload = ocultarTicket
    </script>

</body>

</html>
