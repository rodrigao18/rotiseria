<?php
        session_start();

        if (!isset($_SESSION["user"])) {
            ?>
<script>
	window.location.href = "login.php";

</script>
<?php
        exit();
        }
        $user           = $_SESSION["user"];
        $correo         = $_SESSION["correo"];
        $idVendedor	    = $_SESSION['idVendedor'];
        $nivel          = $_SESSION['nivel'];
		include "php/consulta.php";
        $sql = "SELECT id, tipo_turno FROM turnos WHERE fecha_termino_turno is NULL";
        $turno = consultar($sql);
        $idTurno=0;
        $idTipoTurno=0;
		$tipoTurno='';
		$copyright='<i class="fas fa-copyright"></i>	2019 Copyright Todos los derechos reservados -
		 <i class="fas fa-envelope"></i>  andres.chapa@yoimplemento.cl </p>';

?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
	<aside class="app-sidebar">
		<div class="app-sidebar__user "><i class="fas fa-store fa-2x"></i>
			<div>
				<p class="app-sidebar__user-name pl-3">
					<?php echo $user; ?>
				</p>
				<p class="app-sidebar__user-designation pl-3">
					<?php if ($nivel==0) {
    echo "Administrador";
} elseif ($nivel==1) {
    echo "Vendedor";
} elseif ($nivel==2) {
    echo "Usuario";
}?>
				</p>
				<p class="app-sidebar__user-designation pl-3">
					<?php if (count($turno) > 0) {
    $idTurno=$turno[0]['id'];
    $idTipoTurno=$turno[0]['tipo_turno'];
    if ($turno[0]['tipo_turno']==1) {
        $tipoTurno='mañana';
    } else {
        $tipoTurno='tarde';
    }
    echo "Turno: ". $tipoTurno . "(".$turno[0][0].")" ;
} else {
    echo "Turno : Sin turno";
    $idTurno=0;
    $idTipoTurno=0;
}
                              ?>

				</p>				
			</div>
		</div>
		<ul class="app-menu">
			<li><a class="app-menu__item" href="index.php"><i class="app-menu__icon fas fa-tachometer-alt"></i><span class="app-menu__label">Inicio</span></a></li>
<!-- Ventas -->
			<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-store"></i><span class="app-menu__label">Ventas</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_ventas.php"><i class="icon far fa-check-circle"></i> Ver ventas</a></li>
					<li><a class="treeview-item" href="ingresar_ventas.php"><i class="icon fas fa-save"></i> Ingresar ventas </a></li>
					

				</ul>

			</li>

			<!-- Ordenes de compra -->

			<?php
            $menuOrden='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-truck-loading"></i><span class="app-menu__label">Ordenes de compra</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_compras.php"><i class="icon far fa-check-circle"></i> Ver ordenes de compra</a></li>
					<li><a class="treeview-item" href="seleccionar_proveedor_orden.php"><i class="icon fas fa-plus-square"></i> Ingresar orden de compra </a></li>
				</ul>
			
			</li>';
            if ($nivel==0) {
                echo "";
            } else {
                echo "";
            }
            ?>


			<!--Productos-->
			<?php
            $menuProductos='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-shopping-cart"></i><span class="app-menu__label">Productos </span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_productos.php"><i class="icon far fa-check-circle"></i> Ver productos </a></li>
					<li><a class="treeview-item" href="ver_productos_descartados.php"><i class="icon far fa-times-circle"></i> Ver productos descartados </a></li>
					<li><a class="treeview-item" href="ingresar_productos.php"><i class="icon fas fa-cart-plus"></i> Ingresar productos </a></li>

				</ul>
			</li>';
            if ($nivel==0) {
                echo "";
            } else {
                echo "";
            }
            ?>

			<!--Productos nuevos--->
			<?php
            $menuProductosNuevos='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-shopping-cart"></i><span class="app-menu__label">Productos </span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_productos.php"><i class="icon far fa-check-circle"></i> Ver productos </a></li>
					<li><a class="treeview-item" href="ingresar_productos_nuevos.php"><i class="icon fas fa-save"></i> Ingresar productos </a></li>
					<li><a class="treeview-item" href="ver_categorias_productos.php"><i class="icon fas fa-save"></i> Ver categorias productos </a></li>

				</ul>
			</li>';
            if ($nivel==0) {
                echo $menuProductosNuevos;
            } else {
                echo "";
            }
            ?>

			<?php
			$menuInventario='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-clipboard-list"></i><span class="app-menu__label">Inventario </span><i class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="ver_inventarios.php"><i class="icon far fa-check-circle"></i> Ver registros </a></li>
				<li id="ingresarProductos"><a class="treeview-item" href="ingresar_inventario.php"><i class="icon fas fa-clipboard-check"></i> Ingresar inventario </a></li>
				
			</ul>
			</li>';
			if ($nivel==1 || $nivel==0) {
			echo $menuInventario;
			} else {
			echo "";
			}
			?>
			
			<!--Proveedores--->
			<?php
            $menuProveedores='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-truck"></i><span class="app-menu__label">Proveedores</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_proveedores.php"><i class="icon far fa-eye"></i> Ver proveedores</a></li>
					<li><a class="treeview-item" href="ingresar_proveedores.php"><i class="icon fa fa-plus-square"></i> Ingresar proveedores</a></li>

				</ul>
			</li>';
            if ($nivel==0) {
                echo "";
            } else {
                echo "";
            }
            ?>

			<!---Clientes -->
			<!-- <?php $traspasarClientes='<li><a class="treeview-item" href="traspasar_cliente.php"><i class="icon fa fa-user-tag"></i> Traspasar clientes</a></li>'; ?>
			<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Clientes</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_clientes.php"><i class="icon far fa-eye"> </i> Ver clientes</a></li>
					<li><a class="treeview-item" href="ingresar_clientes.php"><i class="icon fa fa-user-plus"></i> Ingresar clientes</a></li>
					<?php if ($nivel==0) {
                echo $traspasarClientes;
            }?>

				</ul>
			</li> -->



			<!-- Vendedores -->

			<?php
            $menuVendedores='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-users"></i><span class="app-menu__label">Vendedores</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<li><a class="treeview-item" href="ver_vendedores.php"><i class="icon far fa-check-circle"></i> Ver vendedores</a></li>
					<li><a class="treeview-item" href="ingresar_vendedores.php"><i class="icon fas fa-user-plus"></i> ingresar vendedor</a></li>
				
				</ul>
			</li>';
            if ($nivel==0) {
                echo $menuVendedores;
            } else {
                echo "";
            }
            ?>


			<!--Estadisticas-->
			<?php
			$menuEstadisticas='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-chart-bar"></i><span class="app-menu__label">Estadisticas</span><i class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
			<li><a class="treeview-item" href="graficos_vendedor.php"><i class="icon fas fa-chart-pie"></i>Graficos vendedor</a></li>
			<li><a class="treeview-item" href="index_productos_vendidos.php"><i class="icon fas fa-chart-area "></i>Productos vendidos</a></li>

			</ul>
			</li>';
			if ($nivel==0) {
			echo "";
			} else {
			echo "";
			}

            ?>


<?php
$menuFlujoCajas='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-chart-line"></i><span class="app-menu__label">Flujo de cajas</span><i class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
			<li><a class="treeview-item" href="resultado_operacional.php"><i class="icon far fas fa-clipboard-check"></i>  Ingresar gastos generales</a></li>		
			<li><a class="treeview-item" href="ver_categorias.php"><i class="icon far fa-eye"></i> Ver categorias de gastos</a></li>

			</ul>
			</li>';
if ($nivel==0) {
    echo $menuFlujoCajas;
} else {
    echo "";
}

?>

 <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-clock"></i><span class="app-menu__label">Turnos</span><i class="treeview-indicator fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
						<li id="inicioTurnoLink"><a class="treeview-item" href="inicio_turno.php"><i class="icon far fa-eye"></i> Inicio turno</a></li>
						<li id="cambioTurnoLink"><a class="treeview-item" href="cambio_turno.php"><i class="icon fas fa-user-plus"></i> Cambio de turno </a></li>
						<li id="finTurnoLink"><a class="treeview-item" href="fin_turno.php"><i class="icon fas fa-chart-pie"></i> Fin de turno </a></li>

				</ul>
			</li>
			<?php
$menuRespaldarBase='<li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-database"></i><span class="app-menu__label">Respaldar base</span><i class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
			<li><a class="treeview-item" href="respaldar_base.php"><i class="icon fas fa-cloud-download-alt"></i> Guardar archivo</a></li>
			

			</ul>
			</li>';
if ($nivel==0) {
    echo $menuRespaldarBase;
} else {
    echo "";
}

            ?>			

		</ul>
	
	</aside>
	<script>

var ID_TURNO = <?php echo $idTurno;?>;
var TIPO_TURNO = <?php echo $idTipoTurno;?>;
var ID_VENDEDOR = <?php echo $idVendedor; ?>;
var NIVEL = <?php echo $nivel;?>;
var POS=3;
console.log(ID_TURNO);

if(ID_TURNO==0){

   document.getElementById('cambioTurnoLink').style.display ='none';
   document.getElementById("finTurnoLink").style.display ='none';
}

if(TIPO_TURNO==1){
   document.getElementById("inicioTurnoLink").style.display ='none';

}
if(TIPO_TURNO==2){
   document.getElementById("inicioTurnoLink").style.display ='none';
   document.getElementById("cambioTurnoLink").style.display ='none';
}
if(NIVEL==1){
   document.getElementById("ingresarProductos").style.display ='none';
   //document.getElementById("importar").style.display ='none';
}

</script>