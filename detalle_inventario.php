<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistema View Point</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
    <!-- Font-icon css-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css">

    

    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/ventas.css">
</head>

<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>

    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content" id="imprimeme">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-list-ul"></i> Detalle inventario </h1>
                <p>Ver detalle del inventario</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Ver inventario</li>
                <li class="breadcrumb-item active"><a href="#">Detalle inventario</a></li>
            </ul>


        </div>
        <!--Codigo responsivo donde tengo la tabla-->

        <div class="row">
            <div class="col-md-12">
                <div class="tile">

                    <div class="float-right">
												<div id="fecha"></div>
												<div id="hora"></div>
												<div id="fechaFormateada" hidden></div>

                        <br>
                    </div><br>

                    <div class="tile-body">
                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            <form method="POST" action="imprime.php" target="_blank">
                                <input type="hidden" for name="cotizacionNumero" value="<?php echo $_POST[ 'cotizacionNumero']; ?>">
                                <input type="hidden" for name="id" value="<?php echo $_POST['id']; ?>">
                                <input type="hidden" for name="estadoVenta" value="<?php echo $_POST['estadoVenta']; ?>">

                            </form>
                            <h6 class="border-bottom border-gray pb-2 mb-0 " id="numeroVenta">Detalle inventario n° <span id="numInv"></span></h6>

                            <br><br>
                            <!--  datos del cliente -->
                            <div id="salida">
                                <form method="POST" id="formularioEd">
                                    <input type="hidden" class="form-control" id="estadoVenta" name="estadoVenta">

                                    <div class="form-row">

                                        <div class="form-group col-md-12">
                                            <label>Vendedor </label>
                                            <input type="text" class="form-control" id="vendedor" name="vendedor" disabled>
                                        </div>
                                        
                                    </div>               


                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <div class="tile">

                    <table class="table table-striped  table-responsive" id="tabla_detalle_inventario">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" width="5%">Item</th>
                                <th scope="col" width="10%">Código</th>
                                <th scope="col" width="35%">Nombre</th>
                                <th scope="col" width="15%"> Stock anterior </th>
                                <th scope="col" width="15%">Cantidad ingresada</th>
                                <th scope="col" width="10%">Stock actual</th>
                            </tr>
                        </thead>
                        <tbody id="tablaBodyVentas"></tbody>
                    </table>          
                    <div class="btn-group inline float-left" id="volverBtn">
                        <a href="ver_inventarios.php" class="btn btn-info float-left"><i class="fas fa-angle-left fa-1x"></i> volver</a>&nbsp;
                    </div>                 
                    <br><br>
                </div>
            </div>
            <!-- fin col -->

            <!-- fin row -->
        </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>    
    <script type="text/javascript" src="js/detalle_inventario.js?vknet28"></script>
    <script type="text/javascript" src="js/funciones.js?vknet28"></script>


    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
    <!--<script type="text/javascript" src="js/plugins/moment.min.js"></script>-->
    <?php include "./js/table.php"; ?>


	<script type="text/javascript"> 
	     
        var NUMEROINVENTARIO= <?php echo $_POST['numeroInventario'];?>;
		var NIVEL = <?php echo $nivel; ?>;
		var ID_VENDEDORLOGUEADO =<?php echo $idVendedor;?>;
     //   var ESTADOVENTA = <?php echo $_POST['estadoVenta'];?>;
    	  window.onload = cargarDatosIniciales();
    </script>
</body>

</html>
