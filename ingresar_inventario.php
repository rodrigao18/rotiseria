<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistema View Point</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
    <!-- Font-icon css-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css">
</head>

<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-cart-plus"></i> Inventario </h1>
                <p>Ingresar stock de productos</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Inventario</li>
                <li class="breadcrumb-item active"><a href="#">Ingresar Inventario</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                        <form method="POST" id="formularioGuardar">



                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fa fa-search" aria-hidden="true"></i></div>
                                </div>
                                <input type="text" class="form-control" id="buscarCodigoBarra" onkeypress="validarProductoExiste(event);" placeholder="Ingrese codigo de barras">
                            </div>


                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                        <form method="POST" id="formularioGuardar">
                            <div id="salidaProducto"></div>
                            <br>
                            <button id="btnAgregar" class="btn btn-primary float-right" onclick="agregarStock(event)" disabled><i class="fa fa-save"></i> Agregar stock</button>
                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->                    
                </div>

                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                    <input type="hidden" id="idInventario" value="0">
                    <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>                                     
                                        <th scope="col" width="10%">Item</th>
                                        <th scope="col" width="10%">Código</th>
                                        <th scope="col" width="40%">Nombre</th>
                                        <th scope="col" width="10">Stock anterior </th>
                                        <th scope="col" width="10">Cantidad ingresada</th>
                                        <th scope="col" width="10">Stock actual </th>
                                        </tr>
                                    </thead>
                                <tbody id="tablaBodyInventario"></tbody>
                                </table>
                    </div>
                    </div>
                    </div>
            </div>
        </div>

    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/ingresar_inventario.js?vknet28"></script>
    <script type="text/javascript" src="js/funciones.js?vknet28"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>

    <script>
var ID_VENDEDOR =<?php echo $idVendedor;?>; 
        var ID_TURNO = <?php echo $idTurno;?>;

    </script>

</body>

</html>
